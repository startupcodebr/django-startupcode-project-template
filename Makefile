
PROJECT_NAME=`basename "$PWD"`

freeze:
	pipenv lock -r > requirements.txt

migrate:
	python manage.py makemigrations
	python manage.py migrate

migrate_development:
	rm -f development.sqlite3
	python manage.py makemigrations
	python manage.py migrate
	python manage.py loaddata fixtures/development.json.bz2

data_development:
	python manage.py dumpdata --indent=4 -e sessions -e admin -e contenttypes -e auth.Permission > fixtures/development.json
	bzip2 -f fixtures/development.json

load_data_development:
	python manage.py loaddata fixtures/development.json.bz2

clean_migrations:
	find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
	find . -path "*/migrations/*.pyc"  -delete

test_development:
	clean_migrations migrate_development

coverage:
	coverage run manage.py test -k
	coverage report -m

run:
	python manage.py runserver 0.0.0.0:8000

get_static:
	python manage.py collectstatic -c

generate_uml:
	python manage.py graph_models -a -g -o model.png
