

# Django 2.0+ project template

[![Build Status](https://travis-ci.org/dmux/django-startupcode-project-template.svg?branch=master)](https://travis-ci.org/dmux/django-startupcode-project-template)

This is a simple Django 2.0+ project template with my preferred setup. Most Django project templates make way too many assumptions or are just way too complicated. I try to make the least amount of assumptions possible while still trying provide a useful setup. Most of my projects are deployed to Heroku, so this is optimized for that but is not necessary. (https://github.com/dmux/django-startupcode-project-template)

## Features

- Django 2.0+
- Uses [Pipenv](https://github.com/kennethreitz/pipenv) - the officially recommended Python packaging tool from Python.org.
- Development, Staging and Production settings with [django-configurations](https://django-configurations.readthedocs.org).
- Get value insight and debug information while on Development with [django-debug-toolbar](https://django-debug-toolbar.readthedocs.org).
- Collection of custom extensions with [django-extensions](http://django-extensions.readthedocs.org).
- HTTPS and other security related settings on Staging and Production.
- Procfile for running gunicorn with New Relic's Python agent.
- PostgreSQL database support with psycopg2-binary.
- Uses [Django REST framework](http://www.django-rest-framework.org/) - is a powerful and flexible toolkit for building Web APIs.
- Uses [django-sendgrid-v5](https://github.com/sklarsa/django-sendgrid-v5) - An implementation of Django's EmailBackend compatible with sendgrid-python v5+
- Django-Crudbuilder allows you to generate class based views for CRUD (Create, Read, Update and Delete) for specific model - [django-crudbuilder](https://django-crudbuilder.readthedocs.io)

## How to install

```bash
$ django-admin.py startproject \
  --template=https://github.com/dmux/django-startupcode-project-template/archive/master.zip \
  --name=Procfile \
  --extension=py,md,env \
  project_name
$ cp example.env .env
$ pipenv install --dev
```

## Environment variables

These are common between environments. The `ENVIRONMENT` variable loads the correct settings, possible values are: `DEVELOPMENT`, `STAGING`, `PRODUCTION`.

```
ENVIRONMENT='DEVELOPMENT'
DJANGO_SECRET_KEY='dont-tell-eve'
DJANGO_DEBUG='yes'
AZURE_ACCOUNT_NAME=''
AZURE_ACCOUNT_KEY=''
SENDGRID_API_KEY=''
SENTRY_DSN=''
```

These settings(and their default values) are only used on staging and production environments.

```
DJANGO_SESSION_COOKIE_SECURE='yes'
DJANGO_SECURE_BROWSER_XSS_FILTER='yes'
DJANGO_SECURE_CONTENT_TYPE_NOSNIFF='yes'
DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS='yes'
DJANGO_SECURE_HSTS_SECONDS=31536000
DJANGO_SECURE_REDIRECT_EXEMPT=''
DJANGO_SECURE_SSL_HOST=''
DJANGO_SECURE_SSL_REDIRECT='yes'
DJANGO_SECURE_PROXY_SSL_HEADER='HTTP_X_FORWARDED_PROTO,https'
```

## Drone CI

[Drone](https://drone.io) is a Continuous Delivery platform built on Docker, written in Go.

### Enviroment Variables Drone

Variables used in the drone.io continuous integration process:

```
$HEROKU_APP_STAGING
$HEROKU_APP_PRODUCTION
$SLACK_WEBHOOK
$SMTP_HOST
$SMTP_MAIL_USER
$SMTP_MAIL_PASSWORD
$SMTP_MAIL_FROM
$SMTP_MAIL_RECIPIENTS
```

## Deployment

It is possible to deploy to Heroku or to your own server.

### Heroku

```bash
$ heroku create
$ heroku addons:add heroku-postgresql:hobby-dev
$ heroku addons:add newrelic
$ heroku pg:promote DATABASE_URL
$ heroku config:set ENVIRONMENT=PRODUCTION
$ heroku config:set DJANGO_SECRET_KEY=`./manage.py generate_secret_key`
```

## License

The MIT License (MIT)

Copyright (c) 2018 - Startup Code

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
