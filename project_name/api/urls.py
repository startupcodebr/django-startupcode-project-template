# -*- coding: utf-8 -*-
import user_agents
from django.urls import path, include  # NOQA
from django.conf.urls import url
from django.shortcuts import redirect
from rest_framework import permissions
from rest_framework.decorators import api_view
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

API_PREFIX = r'^v(?P<version>[0-9]+\.[0-9]+)'

SchemaView = get_schema_view(
    openapi.Info(
        title="API",
        default_version='1.0',
        description="Documentação da API do projeto {{ project_name }}",
        terms_of_service="https://www.startupcode.com.br",
        contact=openapi.Contact(email="suporte@startupcode.com.br"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,)

)


@api_view(['GET'])
def plain_view(request):
    pass


def root_redirect(request):
    user_agent_string = request.META.get('HTTP_USER_AGENT', '')
    user_agent = user_agents.parse(user_agent_string)

    if user_agent.is_mobile:
        schema_view = 'cschema-redoc'
    else:
        schema_view = 'cschema-swagger-ui'

    return redirect(schema_view, permanent=True)


urlpatterns = [
    url(r'^$', root_redirect),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^swagger(?P<format>.json|.yaml)$',
        SchemaView.without_ui(cache_timeout=0),
        name='schema-json'),
    url(r'^swagger/$',
        SchemaView.with_ui('swagger', cache_timeout=0),
        name='schema-swagger-ui'),  # NOQA
    url(r'^redoc/$',
        SchemaView.with_ui('redoc', cache_timeout=0),
        name='schema-redoc'),
    url(r'^cached/swagger(?P<format>.json|.yaml)$',
        SchemaView.without_ui(cache_timeout=None),
        name='cschema-json'),
    url(r'^cached/swagger/$',
        SchemaView.with_ui('swagger', cache_timeout=None),
        name='cschema-swagger-ui'),  # NOQA
    url(r'^cached/redoc/$',
        SchemaView.with_ui('redoc', cache_timeout=None),
        name='cschema-redoc')
]
