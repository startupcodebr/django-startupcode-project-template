# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import User


class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Informações Pessoais', {'fields': (
            'first_name',
            'last_name',
            'birth_date',
            'bio',
            )}),
        ('Permissões', {'fields': (
            'is_active',
            'is_staff',
            'is_superuser',
            )}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'birth_date', 'password1', 'password2')}
         ),
    )


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
