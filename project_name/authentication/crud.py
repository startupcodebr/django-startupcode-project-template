# -*- coding: utf-8 -*-
from crudbuilder.abstract import BaseCrudBuilder
from .models import User


class UserCrud(BaseCrudBuilder):
    model = User
    search_fields = ['first_name', 'last_name']
    tables2_fields = ('first_name', 'last_name', 'email')
    tables2_css_class = "table table-bordered table-condensed"
    tables2_pagination = 10
    modelform_excludes = ['created_by', 'updated_by']
    login_required = True
    permission_required = True

    @classmethod
    def custom_queryset(cls, request, **kwargs):
        qset = cls.model.objects.filter(created_by=request.user)
        return qset
