# -*- coding: utf-8 -*-
import graphene
import graphql_jwt
from graphene_django.types import DjangoObjectType
from .models import User


class UserType(DjangoObjectType):

    class Meta:
        model = User
        interfaces = (graphene.Node,)


class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info):
        return cls(user=info.context.user)


class Mutation(graphene.ObjectType):
    token_auth = ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


schema = graphene.Schema(mutation=Mutation)
