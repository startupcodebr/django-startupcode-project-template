# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User


class Audit(models.Model):
    created_at = models.DateTimeField(
        auto_now=True,
        verbose_name=_('Created')

    )
    modified_at = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        verbose_name=_('Modified')
    )
    created_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        related_name='%(class)ss_creators',
        on_delete=models.CASCADE,
        verbose_name=_('Created By')
    )
    updated_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        related_name='%(class)ss_updators',
        on_delete=models.CASCADE,
        verbose_name=_('Updated By')
    )

    class Meta:
        abstract = True
