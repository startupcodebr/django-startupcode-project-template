# -*- coding: utf-8 -*-
import graphene
import auth.schema


class Query(
    graphene.ObjectType
):
    pass


class Mutation(
    auth.schema.Mutation,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
