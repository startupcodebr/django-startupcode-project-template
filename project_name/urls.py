# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from graphene_django.views import GraphQLView

admin.site.site_header = 'Backend App'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('crud/',  include('crudbuilder.urls')),
    path('api/', include('{{ project_name }}.api.urls'), name='api'),
    path('ql/', GraphQLView.as_view(graphiql=True))

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
